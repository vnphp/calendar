<?php


namespace Vnphp\Calendar;

use Buzz\Browser;

class GoogleCalendar implements CalendarInterface
{
    private $calendarId;

    private $apiKey;

    /**
     * @var Browser
     */
    private $browser;

    /**
     * GoogleCalendar constructor.
     * @param $calendarId
     * @param $apiKey
     * @param Browser $browser
     */
    public function __construct($calendarId, $apiKey, Browser $browser = null)
    {
        $this->calendarId = $calendarId;
        $this->apiKey = $apiKey;
        $this->browser = $browser ?: new Browser();
    }

    public function isHoliday(\DateTime $date)
    {
        $items = $this->getItems();
        $itemsByDate = [];
        foreach ($items as $item) {
            $start = $item['start'];
            /* @var $start \DateTime */
            $current = clone $start;
            while ($current < $item['end']) {
                $key = $this->formatAsKey($current);
                $itemsByDate[$key][] = $item;
                $current->modify('+1 day');
            }
        }

        return array_key_exists($this->formatAsKey($date), $itemsByDate);
    }

    protected function formatAsKey(\DateTime $date)
    {
        return $date->format('d.m.Y');
    }

    protected function getItems()
    {
        $url = sprintf('https://www.googleapis.com/calendar/v3/calendars/%s/events', urlencode($this->calendarId));
        $query = [
            'key'        => $this->apiKey,
            'maxResults' => 250,
        ];
        $response = $this->browser->get($url . '?' . http_build_query($query));
        $data = json_decode($response->getContent(), true);
        return array_map([$this, 'parseItem'], $data['items']);
    }

    protected function parseItem(array $item)
    {
        return [
            'start' => $this->parseDate($item['start']['date']),
            'end'   => $this->parseDate($item['end']['date']),
            'title' => $item['summary'],
        ];
    }

    protected function parseDate($date)
    {
        return \DateTime::createFromFormat('Y-m-d', $date);
    }
}
