<?php


namespace Vnphp\Calendar;

use Doctrine\Common\Cache\Cache;

class CacheableCalendar implements CalendarInterface
{
    /**
     * @var CalendarInterface
     */
    private $delegate;

    /**
     * @var Cache
     */
    private $cache;

    /**
     * @var int
     */
    private $ttl;

    /**
     * CacheableCalendar constructor.
     * @param CalendarInterface $delegate
     * @param Cache $cache
     * @param int $ttl
     */
    public function __construct(CalendarInterface $delegate, Cache $cache, $ttl = 0)
    {
        $this->delegate = $delegate;
        $this->cache = $cache;
        $this->ttl = $ttl;
    }


    public function isHoliday(\DateTime $date)
    {
        $cacheKey = md5(__METHOD__ . serialize($date));
        if ($this->cache->contains($cacheKey)) {
            $result = $this->cache->fetch($cacheKey);
        } else {
            $result = $this->delegate->isHoliday($date);
            $this->cache->save($cacheKey, $result, $this->ttl);
        }
        return $result;
    }
}
