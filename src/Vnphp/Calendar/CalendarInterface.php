<?php


namespace Vnphp\Calendar;

interface CalendarInterface
{
    public function isHoliday(\DateTime $date);
}
