# Vnphp Calendar


[![build status](https://gitlab.com/vnphp/calendar/badges/master/build.svg)](https://gitlab.com/vnphp/fragment-bundle/commits/master)
[![code quality](https://insight.sensiolabs.com/projects/4aa3eae5-ffa1-4e56-bbc7-f321c69640ae/big.png)](https://insight.sensiolabs.com/projects/4aa3eae5-ffa1-4e56-bbc7-f321c69640ae)

## Installation 

```
composer require vnphp/calendar --prefer-source
```

You need to have `git` installed. 

## Usage

```php
<?php

$calendar = new \Vnphp\Calendar\GoogleCalendar('uk.ukrainian#holiday@group.v.calendar.google.com', 'your_api_key');
$independenceDay = new \DateTime('24-08-2016');
$calendar->isHoliday($independenceDay);
```
