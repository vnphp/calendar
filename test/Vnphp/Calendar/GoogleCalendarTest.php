<?php

class GoogleCalendarTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \Buzz\Browser|\PHPUnit_Framework_MockObject_MockObject
     */
    private $browser;

    /**
     * @var \Vnphp\Calendar\GoogleCalendar
     */
    private $instance;

    protected function setUp()
    {
        $this->browser = $this->createMock(\Buzz\Browser::class);
        $this->instance = new \Vnphp\Calendar\GoogleCalendar(
            'uk.ukrainian#holiday@group.v.calendar.google.com',
            '',
            $this->browser
        );
    }

    public function testIsHoliday()
    {
        $response = $this->createMock(\Buzz\Message\Response::class);
        $response->expects(static::any())
            ->method('getContent')
            ->will(static::returnValue(file_get_contents(__DIR__ . '/ukrainian_holidays.json')));

        $this->browser->expects(static::any())
            ->method('get')
            ->will(static::returnValue($response));

        $independenceDay = new \DateTime('24-08-2016');
        static::assertTrue($this->instance->isHoliday($independenceDay));

        $genericDay = new \DateTime('25-08-2016');
        static::assertFalse($this->instance->isHoliday($genericDay));
    }
}
